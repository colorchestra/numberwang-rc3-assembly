# That's Numberwang

The gameshow that everyone is talking about on WorkAdventure, now coming to [WorkAdventure](https://workadventu.re/). This map can be edited with [Tiled](https://www.mapeditor.org/).


## Hobbies in Somerset

Julie likes to use a local webserver such as [Site.js](https://sitejs.org#platform-tabs) which serves the `main.json` at `https://localhost/main.json`. She then proceeds to any of the current, open Workadventure instances and appends this to the url: `/_/global/localhost/main.json`.

Simon does not have any hobbies. He just goes to `https://party.tabascoeye.de/_/global/localhost/main.json`.


## Rotate the Board

This project is built by a Gitlab CI hook and uploaded to a secret location that might or might not magically work.

&copy; MMXXwtf 
